import React from 'react'

function Header() {
  return (
    <nav className="navbar navbar-expand-lg pt-lg-4 fixed-top">
            <div className="container text-center">
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample01" aria-controls="navbarsExample01" aria-expanded="false" aria-label="Toggle navigation">
                    <i className="fas fa-bars hum" style={{color: "#fff"}}></i>
                    <img src="assets/images/Icon%20metro-cross.png" className="hum d-none" />
                </button>
                <a className="navbar-brand mx-auto d-block d-lg-none" href="#"><img src="assets/images/Comp_7.gif" className="img-fluid logo" /></a>
                <div className="collapse navbar-collapse" id="navbarsExample01">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <a className="nav-link" href="#roadmap">ROADMAP</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#creator">CREATOR</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link btn-blue" href="#about">ABOUT</a>
                        </li>
                        <li className="nav-item d-block d-lg-none">
                            <a className="nav-link ic" href="#roadmap"><img src="assets/images/youtube.png" className="you" /></a>
                        </li>
                        <li className="nav-item d-block d-lg-none">
                            <a className="nav-link ic" href="#creator"><img src="assets/images/twitter.gif" className="twitter" /></a>
                        </li>
                        <li className="nav-item d-block d-lg-none">
                            <a className="nav-link btn-shop" href="#about">SHOPE NFTS</a>
                        </li>
                    </ul>
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item d-none d-lg-block">
                            <a className="nav-link" href="#roadmap"><img src="assets/images/Comp_7.gif" className="img-fluid logo" /></a>
                        </li>
                    </ul>
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item d-none d-lg-block">
                            <a className="nav-link ic" href="#roadmap"><img src="assets/images/youtube.png" className="you" /></a>
                        </li>
                        <li className="nav-item d-none d-lg-block">
                            <a className="nav-link ic" href="#creator"><img src="assets/images/twitter.gif" className="twitter" /></a>
                        </li>
                        <li className="nav-item d-none d-lg-block">
                            <a className="nav-link btn-shop" href="#about">SHOPE NFTS</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
  )
}

export default Header