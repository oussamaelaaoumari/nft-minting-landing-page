import React from 'react'

function Cta() {
  return (
    <div>
      <div className="header pb-5">
            <div className="container">
                <div className="row pt-5 align-items-center">
                    <div className="col-lg-6 pt-5">
                        <img src="assets/images/img2.gif" className="img-fluid" />
                    </div>
                    <div className="col-lg-6 pt-5 text-white text-center">
                        <h1 className="u">That warm and fuzzy feeling. </h1>
                        <h5 className="pt-4">A hand-drawn 70 piece custom Fuzzies Collection is joining the NFT Space on Opensea. </h5>
                        <h1 className="u">available now!</h1>
                        <div className="pt-4">
                            <a className="button bottom bt-lg" href="#">BUY ON OPENSEA</a>
                        </div>
                    </div>
                </div>
                <div className="pt-4 text-center">
                    <a className="button bottom bt-r" href="#">FOLLOW US ON TWITTER</a>
                </div>
                <div className="pt-4 text-center">
                    <a className="button bottom bt-r" href="#">JOIN THE DISCORD</a>
                </div>
            </div>
        </div>
        <div className="bg-blue">
            <div className="container py-lg-5">
                <div className="text-center text-white">
                    <h1 className="u">What the fuzz are Fuzzies?</h1>
                    <div className="row">
                        <div className="col-lg-9 mx-auto pt-5">
                            <h5>Fuzzies are a custom illustrative world, created by <a href="#" className="yellow-link">Hank Washington</a>, aiming to show culture, hip hop and individuality. Oh yeah, and Fuzz. Through dynamic hand-drawn hairstyles, donut lips, and Cheeto eyebrows, Fuzzies embraces what it means to be on the common ground but having a sense of individuality. These culture-driven figures are here to embrace relatability and representation and to make sure it doesn’t get left behind in the new web3 era. </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default Cta