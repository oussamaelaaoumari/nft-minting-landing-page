import React from 'react'

function ArtEx() {
  return (
    <div class="images">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 col-6 px-0">
                        <img src="assets/images/im1.png" class="img-fluid" />
                    </div>
                    <div class="col-md-4 col-6 px-0">
                        <img src="assets/images/im2.png" class="img-fluid" />
                    </div>
                    <div class="col-md-4 col-6 px-0">
                        <img src="assets/images/im3.png" class="img-fluid" />
                    </div>
                    <div class="col-md-4 col-6 px-0">
                        <img src="assets/images/im4.png" class="img-fluid" />
                    </div>
                    <div class="col-md-4 col-6 px-0">
                        <img src="assets/images/im5.png" class="img-fluid" />
                    </div>
                    <div class="col-md-4 col-6 px-0">
                        <img src="assets/images/im6.png" class="img-fluid" />
                    </div>
                </div>
            </div>
        </div>
  )
}

export default ArtEx