import React from 'react'

function Footer() {
  return (
    <div>
        <div className="contect road-map">
            <div className="container color text-center">
                <h2 className="color">Fuzzies are a vibe! These aren’t characters, this is a culture… this is “us.”</h2>
                <h5 className="color">— Jasmine Maietta, round21</h5>
                <div className="py-lg-5"></div>
                <div className="pt-5">
                    <p>Stay in the Fuzz</p>
                    <h5>Be the first to know about the exciting things to come for our Fuzzie Family.</h5>
                </div>
                <div className="pt-5">
                    <input type="text" className="input" placeholder="Email Address" />
                    <button className="btn btn-y">Sign Up</button>
                </div>
            </div>
        </div>
        <footer>
            <img src="assets/images/img-footer.jpeg" className="img-fluid im" />
            <img src="assets/images/88246.jpg" className="over" />
            <div className="center">
                <a href="#" className="mx-2"><img src="assets/images/youtube.png" /></a>
                <a href="#" className="mx-2"><img src="assets/images/twitter.gif" /></a>
            </div>
        </footer>
    </div>
  )
}

export default Footer