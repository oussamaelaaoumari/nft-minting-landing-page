import React from 'react'

function About() {
  return (
    <div class="mission pad">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 pt-5">
                        <img src="assets/images/gif.gif" class="img-fluid" />
                    </div>
                    <div class="col-lg-6 pt-5">
                        <h1 class="u">Fuzzie Mission</h1>
                        <p class="pt-5">Fuzzies mission starts with the release of 70  piece collection that will be available to holders on November 29th. Obtaining a Fuzzies piece will help create a universe that allows these characters to make an impact through culture and representation in underrepresented areas.<br /><br /> Fuzzies exist for the love of art, culture and texture. Our goal is to make sure the metaverse has representation. We will not only embrace the holders, but we will build a bridge between have and have-nots. </p>
                        <div class="pt-5">
                            <a class="nav-link btn-w" href="#about">FOLLOW US ON TWITTER</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  )
}

export default About