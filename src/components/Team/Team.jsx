import React from 'react'

function Team() {
  return (
    <div className="profile pad">
            <div className="container">
                <div className="row">
                    <div className="col-lg-5 pt-5 text-center">
                        <img src="assets/images/profile.jpg" className="img-fluid" />
                    </div>
                    <div className="col-lg-6 offset-lg-1 pt-5 ">
                        <h1><span className="text-sm">The CreatorHank</span>Washington</h1>
                        <p className="pt-5">In 2018, Hank was working his way back into illustration in the midst of heavy branding work with clients. While exploring digital illustration, he was drawn to the use of texture and vibrant colors. Through commemorative artworks and character exploration, Fuzzies were born. Named by the first client that encouraged that style, Round21, Fuzzies became a universe that allowed interpretation run wild. Cheeto eyebrows, doughnut lips and geometric body structure was the repeated formula to build out the univer. Over time, the purpose of Fuzzies tied itself more and more to the impact of black culture. Inspired by the unique vibes of vintage cartoons like Doug, Foster’s Home for Imaginary Friends, Shark Tale, and even shows like dragonball z and the backyardigans that showed characters who weren’t clearly black but felt like they were part of the culture. Characters like Skeeter from Doug and Wilt from Foster’s Home inspired Hank in a way that culture can be colorful and expressive. Naturally inspired by other artists who interpreted leaders of the culture through different mediums like painting or photography, digital illustration allowed efficient flexibility to show figures in untraditional ways. Swag that Fuzzies embodies allowed each one to have it’s own attitude, principles, and relatability. These tend to be reflections of people Hank encounters in real life. The similarities in body parts and differences of attributes of the Fuzzie characters combines individuality with commonality.<br /><br />

                            The name Fuzzies was given by the good folks at round21, who allowed the Fuzzies style to thrive somewhere other than my laptop or iPad.  </p>
                        <div className="pt-5">
                            <a className="nav-link btn-w" href="#about">FOLLOW</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  )
}

export default Team