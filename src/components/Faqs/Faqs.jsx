import React from 'react'

function Faqs() {
  return (
    <div className="faq road-map">
            <div className="container">
                <div id="accordion">
                    <div className="card">
                        <div className="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <h3 className="svelte-1ykiyx9">When does minting begin?</h3>
                            <i className="fas fa-arrow-down"></i>
                        </div>

                        <div id="collapseOne" className="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div className="card-body">
                                <p className="svelte-1ykiyx9">Initial distribution will begin on February 1st 2022 at 3:00 PM UTC. The ongoing distribution will begin on February 8, 2022 at 3:00 PM UTC, and the reveal will happen by this point in time. The initial distribution will be tiered to the benefit of those who have traded or voted on dYdX.</p>
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header" id="headingTwo"  data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <h3>Who can mint one?</h3>
                            <i className="fas fa-arrow-down"></i>
                        </div>
                        <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div className="card-body">
                                <p className="svelte-1ykiyx9">Governance voters &amp; traders on dYdX, and eventually the general public (if any remain) will be able to mint Hedgies as part of an initial distribution beginning on February 1st at 3:00 PM UTC.</p>
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header" id="headingThree"  data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <h3 className="svelte-1ykiyx9">Why should I hold one?</h3>
                            <i className="fas fa-arrow-down"></i>
                        </div>
                        <div id="collapseThree" className="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div className="card-body">
                                <p className="svelte-1ykiyx9">Hedgie ownership will be verified on the dYdX exchange under user profiles and will serve as identities to early traders and governance participants on the dYdX protocol. Additionally, each user holding a Hedgie will also receive a 1 tier increase in $DYDX fee tier discount on the dYdX protocol (for example, users who hold greater than or equal to 5,000 $DYDX will receive a 15% discount (in tier 4) rather than a normal 10% discount (in tier 3).</p>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-header" id="headingThree1"  data-toggle="collapse" data-target="#collapseThree1" aria-expanded="false" aria-controls="collapseThree">
                            <h3 className="svelte-1ykiyx9">What does it cost?</h3>
                            <i className="fas fa-arrow-down"></i>
                        </div>
                        <div id="collapseThree1" className="collapse" aria-labelledby="headingThree1" data-parent="#accordion">
                            <div className="card-body">
                                <p className="svelte-1ykiyx9">Minting Hedgies will only cost gas, and dYdX Trading Inc. will take no royalties from the collection. Only a 2.5% royalty on secondary transactions will be sent directly to the talented independent artists of the project.</p>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-header" id="headingThree2"  data-toggle="collapse" data-target="#collapseThree2" aria-expanded="false" aria-controls="collapseThree">
                            <h3 className="svelte-1ykiyx9">How can I win one?</h3>
                            <i className="fas fa-arrow-down"></i>
                        </div>
                        <div id="collapseThree2" className="collapse" aria-labelledby="headingThree2" data-parent="#accordion">
                            <div className="card-body">
                                <p className="svelte-1ykiyx9">Hedgies will then be distributed daily starting Thursday, February 8th 3:00 PM UTC as rewards to the top % PNL trader on dYdX, and weekly to champions of the dYdX Trading Leagues.</p>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-header" id="headingThree4"  data-toggle="collapse" data-target="#collapseThree4" aria-expanded="false" aria-controls="collapseThree">
                            <h3 className="svelte-1ykiyx9"> Why did you make this?</h3>
                            <i className="fas fa-arrow-down"></i>
                        </div>
                        <div id="collapseThree4" className="collapse" aria-labelledby="headingThree4" data-parent="#accordion">
                            <div className="card-body">
                                <p className="svelte-1ykiyx9">Hedgies unlocks the next chapter for our community &amp; brand. They will be awarded to champions of the market, and function as the foundation of identity on the dYdX exchange, once profiles launch.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  )
}

export default Faqs