import React from 'react'

function RoadMap() {
  return (
    <div className="road-map">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-lg-8 text-white">
                        <div className="media pt-4">
                            <span className="num1 num">0<sub>%</sub></span>
                            <div className="media-body ">
                                <h5>Presale :</h5>
                                <p className="font-400">We sold 469 Skulls and raised 4,367$ to charity! The first 500 Skulls are special, as they grant their owners the title of Ancient Tavern Dweller with special perks in The Tavern.</p>
                            </div>
                        </div>
                        <div className="media mt-4 br">
                            <span className="num2 num">20<sub>%</sub></span>
                            <div className="media-body">
                                <h5>Launch :</h5>
                                <p className="font-400">
                                    On October 31th, Decentralized Bones Society will go live and you will be able to mint your Skull on the DBS website. The Skulls cost 0.05eth + gas to mint, and are limited to 20 Skulls per transaction.</p>
                            </div>
                        </div>
                        <div className="media mt-4">
                            <span className="num3 num">40<sub>%</sub></span>
                            <div className="media-body ">
                                <h5>The Tavern :</h5>
                                <p className="font-400">Soon after the launch, The Tavern will open it's doors. Skull holders will be able to log in with their wallets, and play as the Skulls they own!
                                    They will live the full tavern experience : drinking beer, partying with others, playing one of many tavern games and exchanging information.</p>
                            </div>
                        </div>
                        <div className="media mt-4">
                            <span className="num4 num">60<sub>%</sub></span>
                            <div className="media-body ">
                                <h5>Off-Series Collection :</h5>
                                <p className="font-400">
                                    After we sell out, we will release an off-series collection of unique Skulls. Some will be airdropped to our holders, the others will be auctioned!</p>
                            </div>
                        </div>
                        <div className="media mt-4">
                            <span className="num5 num">80<sub>%</sub></span>
                            <div className="media-body ">
                                <h5>The Tavern DAO :</h5>
                                <p className="font-400">
                                    Owning a Skull makes you a member of The Tavern DAO. The DAO will have a dedicated room in The Tavern to hold meetings, votes and submit proposals. Members have the right to make propositions, and vote on whether to fund said propositions or not using The Treasury, a community wallet.</p>
                            </div>
                        </div>
                        <div className="media mt-4 ">
                            <span className="num6 num">100<sub>%</sub></span>
                            <div className="media-body ">
                                <h5>Tavern Expansion :</h5>
                                <p className="font-400">
                                    The Tavern will grow and develop in the future, to expand on the lore and gameplay, making it a full fledged game. Look forward to social hierarchy, breeding mechanics and Skull clans!</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 d-none d-lg-block">
                        <img src="assets/images/road-map.png" className="img-fluid" />
                    </div>
                </div>
            </div>
        </div>
  )
}

export default RoadMap