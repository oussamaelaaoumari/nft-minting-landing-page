import logo from './logo.svg';
import './App.css';
import Header from "./components/Header/Header"
import Cta from "./components/CTA/Cta"
import About from "./components/About/About"
import ArtEx from "./components/ArtEx/ArtEx"
import RoadMap from "./components/RoadMap/RoadMap"
import Team from "./components/Team/Team"
import Faqs from "./components/Faqs/Faqs"
import Footer from "./components/Footer/Footer"

function App() {
  return (
    <div className="App">
      <Header />
      <Cta />
      <About />
      <ArtEx />
      <RoadMap />
      <Team />
      <Faqs />
      <Footer />
    </div>
  );
}

export default App;
